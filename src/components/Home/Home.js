import './Home.css'
import { connect } from 'react-redux';
import Grid from '@mui/material/Grid';
import { DataComponent } from '../DataComponent/DataComponent';
import { ChartComponent } from '../ChartComponent/ChartComponent';


 const Home = ({ staticData }) => {

    return (
        <div className='app-background'>
            <Grid container spacing={2} className='grid-parent-margin'>
                <Grid item xs={9} style={{'marginTop': '19px'}}>
                <DataComponent data={staticData}></DataComponent>

                </Grid>
                <Grid item xs={3}>
                    <ChartComponent data={staticData}></ChartComponent>
                </Grid>
            </Grid>
           
        </div>
    )
}

const mapStateToProps = (state) => ({
    staticData: state.staticData,
  });
  
  export default connect(mapStateToProps)(Home);