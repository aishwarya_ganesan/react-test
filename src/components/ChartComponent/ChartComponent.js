import './ChartComponent.css';
import { PieChart } from '@mui/x-charts/PieChart';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import { useState } from 'react';

export const ChartComponent = (props) => {
    const { data } = props;
    let mutualFunds = 0;
    let ETFs = 0;
   
    const valueList = data.map((item, index) => {
        if (index < 4) {
            mutualFunds = mutualFunds + (item.price * item.quantity);
            return index === 3 ? { value: mutualFunds, label: 'Mutual Funds' } : {}
        } else {
            ETFs = ETFs + (item.price * item.quantity);
            return index === data.length - 1 ? { value: ETFs, label: 'ETFs' } : {}
        }
    })
    const chartSeries = valueList.filter(value => Object.keys(value).length !== 0);
  
    const palette = ['#87CEEB', '#c0bb6c'];
    const [assets] = useState('asset-wise')
    return (
        <div className='chart-container'>
            <div className='portfolio-header'>
                <div>Portfolio</div>
                <div>
                <FormControl fullWidth>
        <Select sx={{ boxShadow: 'none', '.MuiOutlinedInput-notchedOutline': { border: 0 } }}
          value={assets}
        >
          <MenuItem value='asset-wise'>Asset-wise</MenuItem>
        </Select>
      </FormControl>
                </div>

            </div>
            <div>
                <PieChart
                    colors={palette}
                    series={[
                        {
                            data: chartSeries,
                            innerRadius: 70,
                            outerRadius: 100,
                            paddingAngle: 0,
                            cornerRadius: -16,
                            startAngle: -90,
                            endAngle: 276,
                            cx: 150,
                            cy: 150,
                        }
                    ]
                    }
                    height={800}
                >

                </PieChart>

            </div>

        </div>
    )
}