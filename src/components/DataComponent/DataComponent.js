import './DataComponent.css';
import { Grid } from '@mui/material';
import { styled } from '@mui/material/styles';
import MenuIcon from '@mui/icons-material/Menu';
import iShares from '../../assets/iShares.png';
import QuantityIcon from '../../assets/coin-icon.png';
import InvestIcon from '../../assets/invest-amount-icon.png';
import LinearProgress, { linearProgressClasses } from '@mui/material/LinearProgress';
import ArrowDropUpIcon from '@mui/icons-material/ArrowDropUp';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import Button from '@mui/material/Button';

const BorderLinearProgress = styled(LinearProgress)(({ theme }) => ({
    height: 10,
    borderRadius: 5,
    [`&.${linearProgressClasses.colorPrimary}`]: {
        backgroundColor: theme.palette.grey[theme.palette.mode === 'light' ? 300 : 800],
    },
    [`& .${linearProgressClasses.bar}`]: {
        borderRadius: 5,
        backgroundColor: theme.palette.mode === 'light' ? '#52b963' : '#308fe8',
    },
}));
export const DataComponent = (props) => {
    const { data } = props;

    return (
        <div>
            {
                data.map(item => {
                    return <Grid container spacing={2} key={item.script} className='grid-container-padding'>
                        <Grid item xs={3} className='grid-item-container margin-left-20'>
                            <div className='menu-container'>
                                <div><MenuIcon></MenuIcon></div>
                                <div className='price-container'>
                                    <div className='script'>{item.script}</div>
                                    <div className='price'><span style={{ 'color': 'rgb(156, 153, 153)' }}>$</span><span style={{ 'color': '#0D98BA' }}>{item.price}</span></div>
                                </div>
                                <div className='iShares'><img src={iShares} alt=''></img></div>
                            </div>
                        </Grid>


                        <Grid item xs={2.5} className='grid-item-container'>
                            <Grid container spacing={2}>
                                <Grid item xs={8}>
                                    <div className='flex-align'>
                                        <div className='flex-align'><img src={QuantityIcon} alt="" className='quantity-icon'></img></div><div className='icon-name'>Quantity</div>
                                    </div>
                                </Grid>
                                <Grid item xs={4}>
                                    <div className='icon-value'>{item.quantity}</div>
                                </Grid>
                            </Grid>
                            <Grid container spacing={2}>
                                <Grid item xs={8}>
                                    <div className='flex-align'>
                                        <div className='flex-align'>@</div><div className='icon-name'>Avg. Cost</div>
                                    </div>
                                </Grid>
                                <Grid item xs={4}>
                                    <div className='icon-value'>${item.avgCost}</div>
                                </Grid>
                            </Grid>
                            <Grid container spacing={2}>
                                <Grid item xs={8}>
                                    <div className='flex-align'>
                                        <div className='flex-align'><img src={InvestIcon} alt="" className='quantity-icon'></img></div><div className='icon-name'>Invested Amt</div>
                                    </div>
                                </Grid>
                                <Grid item xs={4}>
                                    <div className='icon-value'>${item.investedAmount}</div>
                                </Grid>
                            </Grid>
                        </Grid>

                        <Grid item xs={3} className='grid-item-container'>
                            <div className='market-value-container'>
                                <div>Market Value</div>
                                <div>${item.price * item.quantity}</div>
                            </div>
                            <div className='portfolio-container'>
                                <div className='portfolio-name'>% of portfolio value</div>
                                <div>{item.portfolioValue}%</div>
                            </div>
                            <div className='progress-container'>
                                <BorderLinearProgress variant="determinate" value={item.portfolioValue} />
                            </div>

                        </Grid>

                        <Grid item xs={2} className='grid-item-container'>
                            <div className='market-value-container'>
                                <div>unrealized P/L</div>
                                <div>-${item.unrealizedPL}</div>
                            </div>
                            <div className='portfolio-container'>
                                <div className='portfolio-name'>% Return</div>
                                <div>
                                    {item.return > 0 ? <div className='arrow-alignment'>
                                        <ArrowDropUpIcon color='success'></ArrowDropUpIcon>{item.return}%</div> :
                                        <div className='arrow-alignment'>
                                            <ArrowDropDownIcon color='error'></ArrowDropDownIcon>{item.return}%
                                        </div>}
                                </div>
                            </div>
                            <div className='percentage-bar-container'>
                                <div className='percentage-bar'>
                                    {item.return > 0 ? <div
                                        className="positive"
                                        style={{ width: `${item.return}%` }}
                                    ></div> : <div
                                        className="negative"
                                        style={{ width: `${Math.abs(item.return)}%` }}
                                    ></div>}
                                </div>
                            </div>
                        </Grid>

                        <Grid item xs={1} className='grid-item-container'>
                            <div className='sell-buy-container'>
                            <Button variant="outlined" color='warning'>BUY</Button>
                        <Button variant="outlined" color='warning' style={{'marginTop': '5px'}}>SELL</Button>
                            </div>
                        

                        </Grid>
                    </Grid>
                })
            }
        </div>
    )
}