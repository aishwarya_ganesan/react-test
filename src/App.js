
import './App.css';
import { connect } from 'react-redux';
import { loadStaticData } from './redux/actions';
import { useEffect } from 'react';
import  Home  from './components/Home/Home';

const App = ({ loadStaticData }) => {
  useEffect(() => {
    loadStaticData();
  }, [loadStaticData]);

  return (
    <div className="App">
      <Home></Home>
    </div>
  );
};

const mapDispatchToProps = {
  loadStaticData,
};

export default connect(null, mapDispatchToProps)(App);
