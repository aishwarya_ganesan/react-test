import { all } from 'redux-saga/effects';
import { watchLoadStaticData } from './saga';

export default function* rootSaga() {
  yield all([
    watchLoadStaticData(),
  ]);
}