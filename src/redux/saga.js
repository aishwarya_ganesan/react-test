import { put, takeLatest } from 'redux-saga/effects';
import { SET_STATIC_DATA, LOAD_STATIC_DATA } from './actionTypes';
import { DATA_LIST } from '../constants/constants';

function* fetchStaticDataSaga() {
  // Simulate fetching static data from an API
  const staticData = DATA_LIST;

  yield put({ type: SET_STATIC_DATA, payload: staticData });
}

export function* watchLoadStaticData() {
  yield takeLatest(LOAD_STATIC_DATA, fetchStaticDataSaga);
}