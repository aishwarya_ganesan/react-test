import { LOAD_STATIC_DATA, SET_STATIC_DATA } from './actionTypes';

export const loadStaticData = () => ({
  type: LOAD_STATIC_DATA,
});

export const setStaticData = (data) => ({
  type: SET_STATIC_DATA,
  payload: data,
});