import { SET_STATIC_DATA } from './actionTypes';
import { DATA_LIST } from '../constants/constants';

const initialState = {
  staticData: DATA_LIST || null,

};
//JSON.parse(localStorage.getItem('staticData'))

const reducer = (state = initialState, action) => {
  
  switch (action.type) {
    case SET_STATIC_DATA:
      // Save to local storage
      localStorage.setItem('staticData', JSON.stringify(action.payload));
      return {
        ...state,
        staticData: action.payload,
      };
    default:
      return state;
  }
};

export default reducer;